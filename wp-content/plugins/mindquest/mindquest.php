<?php
/**
 * Plugin Name:     MindQuest E-learning
 * Plugin URI:      http://jgsoft.hu/mindquest
 * Description:     This is a lightweight e-learning plugin.
 * Author:          Adam Horvath
 * Author URI:      http://jgsoft.hu
 * Text Domain:     mindquest
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Mindquest
 */

namespace JGSoft\Mindquest;

require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

use JGSoft\Mindquest\Mindquest;

define( 'MINDQUEST_DIR', dirname( __FILE__ ) );

new Mindquest();
