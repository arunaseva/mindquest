<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb6d8b61d74d13aba680855ea12c13df4
{
    public static $prefixLengthsPsr4 = array (
        'J' => 
        array (
            'JGSoft\\Mindquest\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'JGSoft\\Mindquest\\' => 
        array (
            0 => __DIR__ . '/../..' . '/inc',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb6d8b61d74d13aba680855ea12c13df4::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb6d8b61d74d13aba680855ea12c13df4::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
