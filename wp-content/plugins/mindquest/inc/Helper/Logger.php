<?php
/**
 * Copyright: Adam Horvath - https://jgsoft.hu
 * License: GPL - https://wordpress.org/about/gpl
 */

namespace JGSoft\Mindquest\Helper;

class Logger {

	/**
	 * Log error to php_errorlog file in WP root dir.
	 * @param $message
	 * @param bool $duplication - write message twice to error log.
	 */
	static function log($message, $duplication = false) {
		error_log($message);
		if ($duplication) {
			$message = 'Log2: ' . $message . "\n";
			file_put_contents(ABSPATH . 'php_errorlog', $message, FILE_APPEND);
		}
	}

	/**
	 * Duplicate log entries. Duplication sometimes may be useful,
	 * e.g. error_log() function in register_activation_hook() not work. Why???
	 * @param $message
	 */
	static function log2($message) {
		self::log($message, true);
	}

}