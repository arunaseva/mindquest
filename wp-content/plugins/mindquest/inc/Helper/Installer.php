<?php
/**
 * Copyright: Adam Horvath - https://jgsoft.hu
 * License: GPL - https://wordpress.org/about/gpl
 */

namespace JGSoft\Mindquest\Helper;

class Installer {

	private static $isInitialized = false;
	private static $db;
	private static $prefix;
	private static $charsetCollate;

	/**
	 * Static class initializer.
	 */
	private static function initialize() {
		if ( self::$isInitialized ) {
			return;
		}

		global $wpdb;
		self::$db             = $wpdb;
		self::$prefix         = self::$db->prefix;
		self::$charsetCollate = self::$db->get_charset_collate();


		self::$isInitialized = true;
	}

	/**
	 * Run at plugin activation.
	 */
	static function activate() {
		self::initialize();

		self::runInstallQueries();
		self::addRoles();

		Logger::log2( 'activated' );
	}

	/**
	 * Run at plugin deactivation.
	 */
	static function deactivate() {
		self::initialize();

		self::runUninstallQueries();
		self::removeRoles();

		Logger::log2( 'deactivated' );
	}

	/**
	 * Run at plugin uninstall.
	 */
	static function uninstall() {
		self::initialize();
		Logger::log2( 'uninstalled' );
	}

	/**
	 * Read and run SQL queries to create custom tables.
	 */
	private static function runInstallQueries() {
		// Parse queries.
		$queries = self::parseQueries( MINDQUEST_DIR . '/inc/Sql/v-0.1.0-install.sql' );

		// Execute sql queries.
		$params = [
			'{prefix}'          => self::$prefix,
			'{charset_collate}' => self::$charsetCollate,
		];
		foreach ( $queries as $id => $query ) {
			$substitutedQuery = str_replace( array_keys( $params ), array_values( $params ), $query );
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			$ret = dbDelta( $substitutedQuery );
		}
	}

	/**
	 * Read and run SQL queries to remove custom tables.
	 */
	private static function runUninstallQueries() {
		// Parse queries.
		$queries = self::parseQueries( MINDQUEST_DIR . '/inc/Sql/v-0.1.0-uninstall.sql' );

		// Execute sql queries.
		$params = [
			'{prefix}' => self::$prefix,
		];
		foreach ( $queries as $id => $query ) {
			$substitutedQuery = str_replace( array_keys( $params ), array_values( $params ), $query );
			self::$db->query( $substitutedQuery );
		}
	}

	/**
	 * Extract queries (identified with ids) from text file.
	 *
	 * @param $filePath
	 *
	 * @return array
	 */
	private static function parseQueries( $filePath ) {
		$queries = [];
		$sqlTxt  = file_get_contents( $filePath );
		$sqlArr  = preg_split( '/\/\*(.|[\r\n])*?\*\//', $sqlTxt ); // Split by comments.
		$sqlArr  = array_map( 'trim', $sqlArr );
		foreach ( $sqlArr as $item ) {
			$firstLine = strstr( $item, "\n", true );
			$id        = null;
			if ( $firstLine ) {
				preg_match( '/id:\s*([a-zA-Z0-9_]+)\s*/', $firstLine, $matches ); // Get id.
				if ( isset( $matches[1] ) ) {
					$id = $matches[1];
				}
			}
			if ( ! $id ) {
				continue;
			}
			// Get string after remove first line with id comment.
			$queries[ $id ] = trim( preg_replace( '/^.+\n/', '', $item ) );
		}

		return $queries;
	}

	/**
	 * Add student and teacher roles and capabilities.
	 */
	private static function addRoles() {
		if (get_role( 'student') === null) {
			$subscriberRole = get_role( 'subscriber' );
			add_role( 'student', 'Student', $subscriberRole->capabilities );
		} else {
			Logger::log2('Role (student) exists, can not be created!');
		}
		if (get_role( 'teacher') === null) {
			$editorRole = get_role( 'editor' );
			add_role( 'teacher', 'Teacher', $editorRole->capabilities );
		} else {
			Logger::log2('Role (teacher) exists, can not be created!');
		}
	}

	/**
	 * Remove student and teacher roles and capabilities.
	 */
	private static function removeRoles() {
		if (get_role('student')) {
			remove_role('student');
		}
		if (get_role('teacher')) {
			remove_role('teacher');
		}
	}

}