<?php
/**
 * Copyright: Adam Horvath - https://jgsoft.hu
 * License: GPL - https://wordpress.org/about/gpl
 */

namespace JGSoft\Mindquest\Helper;

class Database {

	private static $isInitialized = false;
	private static $db;
	private static $prefix;

	/**
	 * Static class initializer.
	 */
	private static function initialize() {
		if ( self::$isInitialized ) {
			return;
		}

		global $wpdb;
		self::$db     = $wpdb;
		self::$prefix = self::$db->prefix;

		self::$isInitialized = true;
	}

	/**
	 * Generate random answer id. Check existing answer ids of the question to avoid duplication.
	 *
	 * @return string - random string between aa-zz from the allowed characters.
	 */
	static function pickNewAnswerId( $questionId ) {
		self::initialize();

		$letters = 'abcdefghjkmnopqrstuvwxyz';
		$pool    = str_split( $letters );

		$table       = self::$prefix . 'mq_answer';
		$existingIds = self::$db->get_col( self::$db->prepare(
			"SELECT id FROM $table WHERE question_id=%d",
			$questionId
		) );

		do {
			$id = '';
			for ( $i = 0; $i < 2; $i ++ ) {
				$id .= $pool[ mt_rand( 0, count( $pool ) - 1 ) ];
			}
		} while (in_array($id, $existingIds));

		return $id;
	}

}