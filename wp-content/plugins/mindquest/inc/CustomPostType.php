<?php
/**
 * Copyright: Adam Horvath - https://jgsoft.hu
 * License: GPL - https://wordpress.org/about/gpl
 */

namespace JGSoft\Mindquest;

use JGSoft\Mindquest\Helper\Logger;

class CustomPostType {

	private static $isInitialized = false;
	private static $types;

	/**
	 * Static class initializer.
	 */
	private static function initialize() {
		if ( self::$isInitialized ) {
			return;
		}

		self::$types = [
			'class',
			'audio',
			'question',
			'quiz',
		];

		self::$isInitialized = true;
	}

	/**
	 * Register custom post types. Call in init hook.
	 */
	static function registerPostTypes() {
		self::initialize();

		// Class post type
		$labels = [
			'name'               => _x( 'Classes', 'class post type general name', 'mindquest' ),
			'singular_name'      => _x( 'Class', 'class post type singular name', 'mindquest' ),
			'menu_name'          => _x( 'Classes', 'class admin menu', 'mindquest' ),
			'name_admin_bar'     => _x( 'Class', 'add new class on admin bar', 'mindquest' ),
			'add_new'            => _x( 'Add new', 'add new class', 'mindquest' ),
			'add_new_item'       => __( 'Add new class', 'mindquest' ),
			'new_item'           => __( 'New class', 'mindquest' ),
			'edit_item'          => __( 'Edit class', 'mindquest' ),
			'view_item'          => __( 'View class', 'mindquest' ),
			'all_items'          => __( 'All classes', 'mindquest' ),
			'search_items'       => __( 'Search classes', 'mindquest' ),
			'parent_item_colon'  => __( 'Parent classes:', 'mindquest' ),
			'not_found'          => __( 'No classes found.', 'mindquest' ),
			'not_found_in_trash' => __( 'No classes found in Trash.', 'mindquest' ),
		];
		$args   = [
			'labels'          => $labels,
			'show_ui'         => true,
			'show_in_menu'    => true,
			'query_var'       => true,
			'rewrite'         => [ 'slug' => 'class' ],
			'capability_type' => 'post',
			'has_archive'     => false,
			'hierarchical'    => false,
			'show_in_rest'    => false,
			'menu_position'   => null,
			'supports'        => [ 'title', 'editor', 'author', 'comments' ],
		];
		register_post_type( 'mq_class_cpt', $args );

		// Audio post type
		$labels = [
			'name'               => _x( 'Audios', 'audio post type general name', 'mindquest' ),
			'singular_name'      => _x( 'Audio', 'audio post type singular name', 'mindquest' ),
			'menu_name'          => _x( 'Audios', 'audio admin menu', 'mindquest' ),
			'name_admin_bar'     => _x( 'Audio', 'add new audio on admin bar', 'mindquest' ),
			'add_new'            => _x( 'Add new', 'add new audio', 'mindquest' ),
			'add_new_item'       => __( 'Add new audio', 'mindquest' ),
			'new_item'           => __( 'New audio', 'mindquest' ),
			'edit_item'          => __( 'Edit audio', 'mindquest' ),
			'view_item'          => __( 'View audio', 'mindquest' ),
			'all_items'          => __( 'All audios', 'mindquest' ),
			'search_items'       => __( 'Search audios', 'mindquest' ),
			'parent_item_colon'  => __( 'Parent audios:', 'mindquest' ),
			'not_found'          => __( 'No audios found.', 'mindquest' ),
			'not_found_in_trash' => __( 'No audios found in Trash.', 'mindquest' ),
		];
		$args   = [
			'labels'          => $labels,
			'show_ui'         => true,
			'show_in_menu'    => true,
			'query_var'       => true,
			'rewrite'         => [ 'slug' => 'audio' ],
			'capability_type' => 'post',
			'has_archive'     => false,
			'hierarchical'    => false,
			'show_in_rest'    => false,
			'menu_position'   => null,
			'supports'        => [ 'title', 'editor', 'author', 'comments' ],
		];
		register_post_type( 'mq_audio_cpt', $args );

		// Question post type
		$labels = [
			'name'               => _x( 'Questions', 'question post type general name', 'mindquest' ),
			'singular_name'      => _x( 'Question', 'question post type singular name', 'mindquest' ),
			'menu_name'          => _x( 'Questions', 'question admin menu', 'mindquest' ),
			'name_admin_bar'     => _x( 'Question', 'add new question on admin bar', 'mindquest' ),
			'add_new'            => _x( 'Add new', 'add new question', 'mindquest' ),
			'add_new_item'       => __( 'Add new question', 'mindquest' ),
			'new_item'           => __( 'New question', 'mindquest' ),
			'edit_item'          => __( 'Edit question', 'mindquest' ),
			'view_item'          => __( 'View question', 'mindquest' ),
			'all_items'          => __( 'All questions', 'mindquest' ),
			'search_items'       => __( 'Search questions', 'mindquest' ),
			'parent_item_colon'  => __( 'Parent questions:', 'mindquest' ),
			'not_found'          => __( 'No questions found.', 'mindquest' ),
			'not_found_in_trash' => __( 'No questions found in Trash.', 'mindquest' ),
		];
		$args   = [
			'labels'          => $labels,
			'show_ui'         => true,
			'show_in_menu'    => true,
			'query_var'       => true,
			'rewrite'         => [ 'slug' => 'question' ],
			'capability_type' => 'post',
			'has_archive'     => false,
			'hierarchical'    => false,
			'show_in_rest'    => false,
			'menu_position'   => null,
			'supports'        => [ 'title', 'editor', 'author', 'comments' ],
		];
		register_post_type( 'mq_question_cpt', $args );

		// Quiz post type
		$labels = [
			'name'               => _x( 'Quizzes', 'quiz post type general name', 'mindquest' ),
			'singular_name'      => _x( 'Quiz', 'quiz post type singular name', 'mindquest' ),
			'menu_name'          => _x( 'Quizzes', 'quiz admin menu', 'mindquest' ),
			'name_admin_bar'     => _x( 'Quiz', 'add new quiz on admin bar', 'mindquest' ),
			'add_new'            => _x( 'Add new', 'add new quiz', 'mindquest' ),
			'add_new_item'       => __( 'Add new quiz', 'mindquest' ),
			'new_item'           => __( 'New quiz', 'mindquest' ),
			'edit_item'          => __( 'Edit quiz', 'mindquest' ),
			'view_item'          => __( 'View quiz', 'mindquest' ),
			'all_items'          => __( 'All quizzes', 'mindquest' ),
			'search_items'       => __( 'Search quizzes', 'mindquest' ),
			'parent_item_colon'  => __( 'Parent quizzes:', 'mindquest' ),
			'not_found'          => __( 'No quizzes found.', 'mindquest' ),
			'not_found_in_trash' => __( 'No quizzes found in Trash.', 'mindquest' ),
		];
		$args   = [
			'labels'          => $labels,
			'show_ui'         => true,
			'show_in_menu'    => true,
			'query_var'       => true,
			'rewrite'         => [ 'slug' => 'quiz' ],
			'capability_type' => 'post',
			'has_archive'     => false,
			'hierarchical'    => false,
			'show_in_rest'    => false,
			'menu_position'   => null,
			'supports'        => [ 'title', 'editor', 'author', 'comments' ],
		];
		register_post_type( 'mq_quiz_cpt', $args );
	}

	/**
	 * Register custom taxonomies for custom post types. Call in init hook.
	 */
	static function registerTaxonomies() {
		self::initialize();

		// Class category taxonomy
		$labels = [
			'name'          => _x( 'Class categories', 'class taxonomy general name', 'mindquest' ),
			'singular_name' => _x( 'Class category', 'class taxonomy singular name', 'mindquest' ),
		];
		$args   = [
			'labels'        => $labels,
			'show_ui'       => true,
			'show_in_menu'  => true,
			'show_in_rest'  => false,
			'show_tagcloud' => false,
			'rewrite'       => [ 'slug' => 'quiz_tax' ],
		];
		register_taxonomy( 'mq_class_tax', 'mq_class_cpt', $args );

		// Audio category taxonomy
		$labels = [
			'name'          => _x( 'Audio categories', 'audio taxonomy general name', 'mindquest' ),
			'singular_name' => _x( 'Audio category', 'audio taxonomy singular name', 'mindquest' ),
		];
		$args   = [
			'labels'        => $labels,
			'show_ui'       => true,
			'show_in_menu'  => true,
			'show_in_rest'  => false,
			'show_tagcloud' => false,
			'rewrite'       => [ 'slug' => 'audio_tax' ],
		];
		register_taxonomy( 'mq_audio_tax', 'mq_audio_cpt', $args );

		// Question category taxonomy
		$labels = [
			'name'          => _x( 'Question categories', 'question taxonomy general name', 'mindquest' ),
			'singular_name' => _x( 'Question category', 'question taxonomy singular name', 'mindquest' ),
		];
		$args   = [
			'labels'        => $labels,
			'show_ui'       => true,
			'show_in_menu'  => true,
			'show_in_rest'  => false,
			'show_tagcloud' => false,
			'rewrite'       => [ 'slug' => 'question_tax' ],
		];
		register_taxonomy( 'mq_question_tax', 'mq_question_cpt', $args );

		// Quiz category taxonomy
		$labels = [
			'name'          => _x( 'Quiz categories', 'quiz taxonomy general name', 'mindquest' ),
			'singular_name' => _x( 'Quiz category', 'quiz taxonomy singular name', 'mindquest' ),
		];
		$args   = [
			'labels'        => $labels,
			'show_ui'       => true,
			'show_in_menu'  => true,
			'show_in_rest'  => false,
			'show_tagcloud' => false,
			'rewrite'       => [ 'slug' => 'quiz_tax' ],
		];
		register_taxonomy( 'mq_quiz_tax', 'mq_quiz_cpt', $args );
	}

}