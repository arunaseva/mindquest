<?php
/**
 * Copyright: Adam Horvath - https://jgsoft.hu
 * License: GPL - https://wordpress.org/about/gpl
 */

namespace JGSoft\Mindquest;

use JGSoft\Mindquest\Helper\Logger;
use JGSoft\Mindquest\CustomPostType;

class Mindquest {

	/**
	 * Mindquest constructor.
	 */
	public function __construct() {
		register_activation_hook( MINDQUEST_DIR . '/mindquest.php', [
			'JGSoft\Mindquest\Helper\Installer',
			'activate'
		] );
		register_deactivation_hook( MINDQUEST_DIR . '/mindquest.php', [
			'JGSoft\Mindquest\Helper\Installer',
			'deactivate'
		] );
		register_uninstall_hook( MINDQUEST_DIR . '/mindquest.php', [
			'JGSoft\Mindquest\Helper\Installer',
			'uninstall'
		] );
		add_action( 'init', [ $this, 'init' ] );
	}

	public function init() {
		load_plugin_textdomain( 'mindquest', false, 'MINDQUEST_DIR' );
		CustomPostType::registerPostTypes();
		CustomPostType::registerTaxonomies();
	}

}

